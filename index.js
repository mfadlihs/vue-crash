const app = Vue.createApp({
  data() {
    return {
      url: "https://www.instagram.com/mfadlihs",
      title: "Dancokkkk",
      author: "The Net Ninja",
      year: 2023,
      isVisible: true,
      books: [
        {
          title: "Book Number 1",
          year: 2001,
          thumbnail: "assets/1.jpg",
          isFav: true,
        },
        {
          title: "Kekuasaan Nabi",
          year: 2018,
          thumbnail: "assets/2.jpg",
          isFav: false,
        },
        {
          title: "Prabowo Kalah",
          year: 2024,
          thumbnail: "assets/3.jpg",
          isFav: true,
        },
      ],
    };
  },
  methods: {
    toggleVisible() {
      this.isVisible = !this.isVisible;
    },
    handleEvent(e) {
      console.log(e);
      console.log("tes mouse enter");
    },
    mouseMove(e) {
      console.log(e.offsetY, e.offsetX);
    },
    toggleFav(e) {
      console.log("copkkkk");
      e.isFav = !e.isFav;
    },
  },
  computed: {
    filteredBook() {
      return this.books.filter((e) => e.isFav);
    },
  },
});

app.mount("#app");
